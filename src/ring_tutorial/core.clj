(ns ring-tutorial.core
  (:require
   [ring.adapter.jetty :as jetty]
   [ring.util.response :as response]))

(defn print-resp [& args]
  (with-out-str (clojure.pprint/pprint args)))

(defn print-duration [handler]
  (fn [request]
    (let [start# (. System (nanoTime))
          resp# (handler request)
          elapsed (/ (double (- (. System (nanoTime)) start#)) 1000000000.0)]
      (println {:elapsed elapsed})
      resp#)))

(defn basic-response [req]
  (response/response (print-resp req)))

(defn app-handler [req]
  ((-> basic-response
       (print-duration)) req))

(defn repl-main []
  (jetty/run-jetty #'app-handler {:port 3011 :daemon? true :join? false}))

(defn -main [& _]
  (jetty/run-jetty #'app-handler {:port 3010}))

((time (+ 1 1)))